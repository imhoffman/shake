# Seismometer Commissioning
The [Raspberry Shake](https://shop.raspberryshake.org/product/magnitude-9-0/) has been situated in its current location at [Quest University Canada](https://questu.ca/) since 2017 Jun 10 (and has undergone all automated software updates since that time). Described here are the efforts to determine whether the Shake is meeting its specifications&mdash;namely to "Record earthquakes magnitude 4 and up within a radius of around 250 miles (400 km)"&mdash;and is generally behaving sensibly.

The general conclusion is that 95% of events of equaivalent moment magnitude *Mw*&asymp;4 can be detected at 400&nbsp;km, with different analyses (click on the following numbers for PDF plots) yielding 99% confidence at distances in the range [290](images/poly.pdf)&ndash;[370](images/linear.pdf)&nbsp;km. The most recent output of the analyses described below may be found in [`summary.txt`](images/summary.txt) and at the very bottom of this README.

## Seismic Data
### [`iris.m`](matlab/iris.m)
The miniSEED files written by the Shake are inspected using a general-purpose interactive visualizer written in Octave/Matlab. The files are read in using a call to [Fran&ccedil;ois Beauducel's](http://www.ipgp.fr/~beaudu/) `rdmseed.m` script available from [Mathworks](https://www.mathworks.com/matlabcentral/fileexchange/28803-rdmseed-and-mkmseed--read-and-write-miniseed-files) or [GitHub](https://github.com/IPGP/mseed-matlab) or [his page at IPGP](http://www.ipgp.fr/~beaudu/matlab.html). NB: Fran&ccedil;ois' scripts take forever to run and it is not clear what they are doing that takes so long.

The user is prompted to enter the UTC date and time of the event as well as the 3D earth coordinates. The date is used to compute the day of the year (which is how the miniSEED files are automatically named). The miniSEED time stamp is decimal days since a past fiducial, meaning that extraordinary bit depth is required to record fractions of a second at the necessary dynamic range. This requirement is worked around by removing several digits of precision from the integer number of days.

The script also uses an internet call to a URL constructed according to [this IRIS utility](http://service.iris.edu/irisws/traveltime/docs/1/builder/) which facilitates an interface to the earth models on their server. The location of the Shake is hard-coded into the URL string in the script. The web return from the IRIS server is parsed into arrival times and labels for the expected waves. The script is written to request none of the "phases" explicitly, which results in whatever the server had been configured to return as a default for that particular angular distance. It is possible to explicitly tailor the returned phases, for which an example URL has been left in a comment in the script. The script uses decimal days throughout and so the times in seconds from IRIS are converted accordingly.

The data are low-pass filtered for display using `fir1` from the [`signal`](https://octave.sourceforge.io/signal/) package. A modest roll-off of order 33 is working well. A `specgram` is also generated using settings appropriate for low frequencies. Deconvolving the instrument response was considered. [Here](images/resp.pdf) is a PDF plot of the [nominal and actual instrument responses](http://manual.raspberryshake.org/specifications.html#raspberry-shake-1d-instrument-response-files) generated using [`resp.m`](matlab/resp.m) on the files [`response_stock.fir`](matlab/response_stock.fir) (obtained by editing the 105 coefficients from the RESP file in the web manual) and [`response.fir`](matlab/response.fir) (obtained by editing the 53 coefficients from the XML file from [the stationview for our device](http://www.raspberryshake.net/stationview/#?net=AM&sta=RBF6B)). The poles and zeros are no different between the two descriptions and do not correspond to the FIR response. In the end, no deconvolution has been performed since both FIR responses are flat through the salient range 0&ndash;8&nbsp;Hz and because ground-motion solutions are not sought.

For [this event in Russia](https://earthquake.usgs.gov/earthquakes/eventpage/us2000a21g#executive), below is a screen grab of the interactive Octave window following the displayed entries at the command-line interface. Also, a PDF of the plotting window may be generated using [`pdfout.m`](pdfout.m)

```
Event UT date (in quotes) as YYYY-MM-DD HH:MM:SS:  '2017-07-28 02:39:16'
Event latitude (decimal deg, North is positive):  54.332
Event longitude (decimal deg, East is positive):  169.277
Event depth (km, down is positive):  10.0
```

<p align="center">
<img src="images/octaveFigure.png" align="center" width="800" />
</p>

#### other selected results
- [Here](images/mozambique.pdf) is a PDF of the remarkable detection through the core of the [intraplate event in Mozambique](https://earthquake.usgs.gov/earthquakes/eventpage/us20009pf9#executive). [Here](images/indian.pdf) is a PDF for [an event with similar propagation from the Indian Ocean](https://earthquake.usgs.gov/earthquakes/eventpage/us2000a5dp#executive). These may be contrasted with [this non-detection](images/philippines.pdf) of [a closer but otherwise comparable event in the Philippines](https://earthquake.usgs.gov/earthquakes/eventpage/us2000a4zc#executive). The simple first- and second-order decision boundaries employed below cannot represent this structure.
- [Here](images/nz.pdf) is a PDF for [an event in New Zealand](https://earthquake.usgs.gov/earthquakes/eventpage/us100098qm#executive).
- [Here](images/indianO.pdf) is a PDF of [an *Mb*=4.9 in the Indian Ocean](https://earthquake.usgs.gov/earthquakes/eventpage/us2000acrg#executive).

The decision of whether an earthquake is detected in the data is a subjective human decision based on viewing the filtered waveform with respect to the IRIS arrival times in the interactive figure window. The figure display includes the separation angle of the event from the Shake as reported from the IRIS server as well as the depth of the event (which otherwise does not enter the calculations).

### [`detect.txt`](fortran/detect.txt)
A human decision based on the Octave display of whether the earthquake appears in the data is recorded as `Y` for yes, `N` for no, or `X` for uncertain owing to background confusion or generally marginal subjective impression. Also recorded for an event is the separation angle (from the Octave display of the IRIS calculation) and the magnitude and magnitude scale from the IRIS summary page&mdash;for example, [this one](https://earthquake.usgs.gov/earthquakes/eventpage/us100096p0#executive). Of the various published magnitude scales, the program currently understands `md`, `mb`, and `ml` for subsequent conversion and all other entries (such as `mw`, `mww`, and `mwr`) are accepted as *Mw* and left unconverted. The file also allows for up to 80 characters of a free-format memo for each entry which is generally used for UTC date/time and an identifying location note. At runtime, there is a header line that is skipped during the read and then one earthquake per line is read using the format `A1,F7.2,F4.1,A4,A80`. The file is ordered by distance for ease of human review&mdash;this ordering is unimportant at runtime (expect that it ensures a random test set for `sens.m` that is well-distributed in angle).

## Ensemble Analysis
### [`sens.f`](fortran/sens.f)
The file of detections are read, processed, and plotted in a fortran program. The program uses the [pgplot](http://www.astro.caltech.edu/~tjp/pgplot/) plotting library as well as the logistic regression ([`logistic.f90`](fortran/logistic.f90)) and least-squares fitting ([`lsq.f90`](fortran/lsq.f90)) routines by Alan J. Miller available from [CSIRO](http://wp.csiro.au/alanmiller/) or [here](http://jblevins.org/mirror/amiller/). A command-line compilation on ubuntu is

```bash
$ gfortran -lm -ltk8.6 sens.f -lpgplot logistic.f90 lsq.f90
```

The resulting executable accepts three arguments.
1. `N` or `L` depending on whether the horizontal (distance/angle) axis in the plots should be normal or logarithmic, respectively.
2. `Y` or `N` depending on whether `X` entries should be treated as non-detections or ignored, respectively.
3. a float in the range (0,1) for the confidence level from the logistic fit to be displayed&mdash;for example, `0.67`.

The `a.out` executable outputs a postscript file of the requested plot. Other pgplot output options are available in comment lines in the code, requiring re-compilation.

#### magnitude conversions
The seismology community employs a variety of magnitude scales with which to quantify event phenomena. There exist only empirical relations among them. The relations in the code are drawn from the following papers.

- B. Lolli, P. Gasperini, G. Vannucci. "Empirical conversion between teleseismic magnitudes (mb and Ms) and moment magnitude (Mw) at the Global, Euro-Mediterranean and Italian scale." Geophys J Int (2015) 200 (1): 199. [doi.org/10.1093/gji/ggu385](https://doi.org/10.1093/gji/ggu385)
- Ranjit Das, H.R.Wason., M.L.Sharma. "Magnitude conversion to unified moment magnitude using orthogonal regression relation." Journal of Asian Earth Sciences, Volume 50, 2 May 2012, Pages 44&ndash;51. [doi.org/10.1016/j.jseaes.2012.01.014](https://doi.org/10.1016/j.jseaes.2012.01.014)
- Scordilis E., Kementzetzidou D., and Papazachos B. "Local Magnitude Estimation in Greece, Based on Recordings of the Hellenic Unified Seismic Network (HUSN)." Bulletin of the Geological Society of Greece, vol. XLVII No. 3 2013 Proceedings of the 13th International Congress, Chania, Sept. 2013 p. 1241
- E. M. Scordilis. "Empirical Global Relations Converting MS and mb to Moment Magnitude." Journal of Seismology, April 2006, Volume 10, Issue 2, pp 225–236. [doi:10.1007/s10950-006-9012-4](doi:10.1007/s10950-006-9012-4)
- Filiz Tuba KADİRİOĞLU, Recai Feyiz KARTAL. "The new empirical magnitude conversion relations using an improved earthquake catalogue for Turkey and its near vicinity (1900–2012)." Turkish J Earth Sci (2016) 25: 300&ndash;310 [doi:10.3906/yer-1511-7](https://doi:10.3906/yer-1511-7)

See also

- Aykut Deniz, M. Semih Yucemen. "Magnitude conversion problem for the Turkish earthquake data." Natural Hazards (2010) 55:333&ndash;352. [doi:10.1007/s11069-010-9531-8](https://doi.org/10.1007/s11069-010-9531-8)
- E. M. Scordilis, D. Kementzetzidou, B. C. Papazachos. "Local magnitude calibration of the Hellenic Unified Seismic Network." J Seismol (2016) 20: 319. [doi:10.1007/s10950-015-9529-5](https://doi.org/10.1007/s10950-015-9529-5)
- John Ristau, Garry C. Rogers, John F. Cassidy. "Moment Magnitude-Local Magnitude Calibration for Earthquakes in Western Canada." Bulletin of the Seismological Society of America (2005) 95(5):1994. [doi:10.1785/0120050028](https://dx.doi.org/10.1785/0120050028)

The various conversions have been built into the code. Some of the different options are commented out for optional later use, in which case the user may change the comments and re-compile&mdash;there is no command-line switch for controlling the various permutations of interconversions.

As noted in the papers, there is a limited range of magnitudes over which the empirical relationships can be trusted. The conversions from low values of `ml` are particularly problematic. On the IRIS event pages, it is possible to click on "Origin" then the tab for "Magnitudes" to check if the event has been quantified on any other scales. Our choice to quantify the sensitivity in *Mw* space was an arbitrary one; the scale used to make the claim about the Shake was not specified.


#### logistic regression
The logistic regression is performed on two features: moment magnitude and the logarithm of the angle. Since the fit is limited to linear trends in each feature, magnitude&ndash;log(angle) space is best suited to the problem, as discussed below.

At the current location of the Shake, the background noise is dominated by a nearby construction site for approximately 25% of the hours in the day. Therefore, in order to investigate the Shake spec, it is appropriate to determine the portion of detection space that corresponds to a 75% detection rate. If the user chooses to include the `X` decisions as non-detections, a solution will be included on the plot for the distance at which 75% of magnitude-4 events will be detected. If the user chooses to ignore `X` events, a solution for 99% success will be quoted.

**As of writing, analysis of approximately 225 events yields a 99% success for detecting *Mw*=4 events at a distance of 390&nbsp;km, consistent with expectations,** as shown below following the execution

```bash
$ ./a.out L N 0.67
```
<p align='center'>
<img src='images/linear.png' width="600">
</p>

#### geometry calculations
A simple geometric model for detection limit as a function of event magnitude and distance is described in [`angle.pdf`](fortran/angle.pdf). Compared with the naive inverse-square model, the logistic regression is suggestive of the seismometer underperforming at large distances and overperforming at short distances. This result is not worth overinterpreting since the magnitude interconversions have near&ndash;far systematics that are likely a limiting factor, though it is useful to find that the logistic fit can be reconciled with physical expectations within an order of magnitude.

The value of the constant *a* in the code is chosen heuristically for display purposes only. If exhaustively interpreted, *a* is related to the absolute sensitivity of the device, especially if the fraction of event energy released as seismic waves is known.


#### [`linear.sh`](fortran/linear.sh)
Because the third-party fitting routine has proven unstable, the polynomial boundary described below was back-ported in order to apply gradient descent to a linear decision boundary. The files [`afterl.f`](fortran/afterl.f), [`testingl.f`](fortran/testingl.f), and [`sensl.m`](matlab/sensl.m) contain the necessary changes. Thus, as long as *ghostscript* and *ghostview* are available, this shell script quickly constructs and displays [a two-sided PDF](images/linear.pdf) of the normal- and log-scale outputs, along with terminal output.

```
Completed in 180704 iterations.
Training set has 474 events including 146 detections.
 Random test set has 53 events including 15 detections.
 Test score: 71.7%
Coefficients and estimated fractional uncertainty:
      constant: -7.84E+00 4.8E-02
    log(angle): -7.91E+00 4.9E-02
     magnitude: +4.16E+00 5.1E-02
```

At time of writing, the linear boundary for **436 events including 125 detections yields a 99% success for detecting *Mw*=4 events at a distance of 430&nbsp;km**.

### polynomial logistic boundary
In this section is described an alternative logistic analysis. A version of the `sens.f` fortran code described above is still used for converting the magnitudes to a common scale, for selecting whether to consider no-decision events, and to plot the models and data in the same pgplot format. The difference from the preceding treatment is that a hyperparabolic decision boundary is determined using a Matlab script.

The third-party `lsq.f90` is not stable for some of the fits&mdash;in particular, non-linear logistic boundaries (for example, boundaries quadratic in either magnitude or distance). The desired model is described in [this pdf](fortran/logit.pdf). In order to explore these higher-order fits, a simple logistic regression was written for Octave using gradient descent. In order to prepare the data for the Octave processing, the file `sens.f` was altered into files for use `before` and `after` the Octave run of `sens.m`.

#### [`before.f`](fortran/before.f)
The program reads `detect.txt` and outputs a simple, three-column data file `detect.dat` of a one or a zero for the detection, the distance angle in degrees, and the adjusted magnitude. It accepts a single argument concerning how no-decision entries should be handled.

```bash
$ gfortran before.f -o before
$ ./before N
```

will generate a `detect.dat` file without entries for no-decisions&mdash;at time of writing, 317 events including 96 detections.

#### [`sens.m`](matlab/sens.m)
With `detect.dat` as an input, the output of `sens.m` is the file [`beta.dat`](fortran/beta.dat)&mdash;a five-element vector of the best-fit coefficients to the polynomial described in [`logit.pdf`](fortran/logit.pdf). In addition to writing the `beta.dat` file, fit diagnostic output is written to the terminal.

The script accepts a single command-line argument, *n*. If desired, a test set is sequestered by selecting every *n* th entry in `detect.dat` from a random starting point among the first several events on the list. The character of the test set and the outcome of the test is displayed on the terminal for the user.

#### [`after.f`](fortran/after.f)
The coefficients in `beta.dat` from `sens.m` are then read into `after` in order to produce a plot comparable to that of `sens.f`.

```bash
$ gfortran -lm -ltk8.6 after.f -lpgplot -o after
$ ./after L N 0.33
```

yields, for example, [this plot](images/munge.pdf), including the report that **99% of *Mw*=4 events will be detected at a distance of 350&nbsp;km.** The second command-line argument `N` is chosen to match the generation of `detect.dat`.

<p align='center'>
<img src='images/munge.png' width="600">
</p>

#### [`testing.f`](fortran/testing.f)
This program makes a pgplot PNG of the logistic model for display on the README: the latest polynomial fit for magnitude-4 sensitivity.

#### [`poly.sh`](fortran/poly.sh)
This shell script accepts two arguments:

1. `Y` or `N` for the handling of the no-decision events
2. `F` or `T` for using either the full data in the analysis or for splitting out a portion for a train/test result

For example,

```
Completed in 156325 iterations.
Training set has 474 events including 146 detections.
 Random test set has 53 events including 15 detections.
 Test score: 71.7%
Coefficients and estimated fractional uncertainty:
      constant: -4.48E+00 9.3E-02
    log(angle): -8.68E+00 1.1E-01
  log(angle)^2: +3.27E-01 7.2E-01
     magnitude: +1.91E+00 1.6E-01
   magnitude^2: +3.28E-01 2.5E-02
```

Use of this script ensures that the no-decision choices match in the runs of `before` and `after`, as they must. The script runs the two fortran programs and the Octave fitter `sens.m` for a test set that is 1/10th of the entire list, if requested. The output is a PDF ([here is the latest](images/poly.pdf)) along with the terminal output of `sens.m`. It also runs `testing.f` to generate the figure below.

<p align='center'>
<img src='images/four.png' width="600">
</p>

#### [`both.sh`](fortran/both.sh)
This script generates the [`summary.txt`](images/summary.txt) file from gradient-descent determinations of both linear and parabolic boundaries, showing the most recent fits to the data.
