       program testl
c
       integer pgbeg, i, n, s4, yn
       parameter ( k = 2048, kk = 26, nn = 3 )
       real a, m, de, dmin, dmax, smin, smax, d4, P4
       real Pmin, Pmax
       real P(k), deg(k), logit(k)
       real legx(2), legy(2), s(kk), theta(kk)
       real lbeta(nn) 
       character*1 choice
       character*5 str
       character*10 dist
       character*39 label
       character*27 shake
       character*80 junk
c constants
       choice = 'L'
       dmin = -08.0
       dmax = 169.0
       Pmin = +0.00
       Pmax = +1.00
c distance axis
       do i = 1, kk
        s(i) =  0.0+ real(i-1)/real(kk)*13000.0
        theta(i) = 2.0*asin(s(i)/2.0/6400.0)*180.0/3.14159
        if ( choice .eq. 'L' ) then
         theta(i) = log10(theta(i))
        end if
       end do
c
c  ---=== plotting ===---
c
c begin pgplot
c       if ( pgbeg(0,'/XWINDOW',1,1) .ne. 1 ) stop
c       if ( pgbeg(0,'out.ps/CPS',1,1) .ne. 1 ) stop
c       if ( pgbeg(0,'out.ps/VCPS',1,1) .ne. 1 ) stop
c       if ( pgbeg(0,'out.gif/GIF',1,1) .ne. 1 ) stop
       if ( pgbeg(0,'four.png/PNG',1,1) .ne. 1 ) stop
c       call pgask ( .false. )
c
       call pgsvp(  0.15, 0.90, 0.15, 0.85 )
       call pgslw(3)
       call pgsch(1.5)
       if ( choice .eq. 'L' ) then
        dmin = -0.82
        call pgswin(  dmin,  log10(dmax),  Pmin, Pmax )
       else if ( choice .eq. 'N' ) then
        call pgswin(  dmin,  dmax,  Pmin, Pmax )
       else
        goto 1000
       endif
       call pglab (
     $           'angular distance from hypocenter (degrees)',
     $           'probability of detection',
     $           '')
c
       call pgscr(03, 0.00, 0.75, 0.00)
       call pgsls(1)
       call pgslw(4)
       call pgsch(1.0)
c logistic fit from octave sens.m
       open ( unit = 15, file="beta.dat" )
       do i = 1, nn
        read(15,*) lbeta(i)
       end do
       close(15)
c model curve
       do i = 1, k
        P(i) = 0.0 + real(i)/real(k)
        deg(i) = (log(P(i)/(1.0-P(i)))-lbeta(1)-lbeta(3)*4.0)/lbeta(2)
        if ( choice .ne. 'L' ) then
         deg(i) = 10**deg(i)
        end if
       end do
       call pgsls(2)
       call pgslw(3)
       call pgsci(08)
       call pgline(k,deg,P)
c Shake spec (only for log display---crowding)
       if ( choice .eq. 'L' ) then
        P4 = 0.99
        deg(i) = (log(P4/(1.0-P4))-lbeta(1)-lbeta(3)*4.0)/lbeta(2)
        d4 = 10**d4 * 3.14159/180.0
        s4 = int(2.0*6400.0*sin(d4/2.0))
        s4 = s4 - mod(s4,10)
        write(dist,'(A3,I4,A3)') 'at ',s4, ' km'
        d4 = d4*180.0/3.14159
        call pgsch(2.0)
        d4 = log10(d4)
        call pgsci(6)
        call pgpt1(d4,Pmin,11)
        call pgpt1(d4,Pmax,11)
        write(shake,'(I2,A25)') int(P4*100.0),
     $               '% of \fiM\fn\dW\u\(0248)4'
        call pgptxt(d4,Pmin+0.20*(Pmax-Pmin),
     $             0.0,0.5,shake)
        call pgptxt(d4,Pmin+0.10*(Pmax-Pmin),
     $             0.0,0.5,dist)
       end if
c non-linear distance axis
       call pgsci(1)
       call pgsch(1.50)
       do i = 1, kk
        call pgpt1(theta(i),Pmax,0808)
        if ( i.eq.2 ) then
         write(str,'(I3)') int(s(i))
         call pgptxt(theta(i),Pmax+0.05*(Pmax-Pmin),0.0,0.50,str)
        end if
        if ( i.eq.3 ) then
         write(str,'(I4)') int(s(i))
         call pgptxt(theta(i),Pmax+0.05*(Pmax-Pmin),0.0,0.50,str)
        end if
        if ( i.eq.1 .or. (mod(i,10).eq.1) ) then
         call pgpt1(theta(i),Pmax,0798)
         write(str,'(I5)') int(s(i))
         call pgptxt(theta(i),Pmax+0.05*(Pmax-Pmin),0.0,0.50,str)
        end if
       end do
       if ( choice .eq. 'L' ) then
        call pgptxt((log10(dmax)-0.15)/2.0,Pmax+0.14*(Pmax-Pmin),0.0,0.5
     $      ,'linear distance from epicenter (km)')
       else
        call pgptxt((dmin+dmax)/2.0,Pmax+0.14*(Pmax-Pmin),0.0,0.5,
     $      'linear distance from epicenter (km)')
       end if
c frame and labels
       call pgsch(1.50)
       call pgsls(1)
       call pgsci(1)
       call pgslw(3)
       if ( choice .eq. 'L' ) then
        call pgbox ( 'bnlts', 0.0, 0, 'bcmtsv', 0.0, 0 )
       else
        call pgbox ( 'bnts', 0.0, 0, 'bcmtsv', 0.0, 0 )
       end if
       call pgsch(1.00)
c other horizontal axis and legend
       smin = dmin*3.14159/180.0*6400.0
       smax = dmax*3.14159/180.0*6400.0
       call pgslw(3)
       call pgsch(1.5)
       call pgswin(  smin,  smax, Pmin, Pmax )
       call pgbox ( 'c', 0.0, 0, '', 0.0, 0 )
       call pgsch(1.25)
       call pgsci(1)
       call pgsci(08)
       legy(1) = Pmax-0.09*(Pmax-Pmin)
       legy(2) = legy(1)
       legx(1) = smax-(0.06+0.01)*(smax-smin)
       legx(2) = smax-(0.06-0.01)*(smax-smin)
       call pgline(2,legx,legy)
       call pgsci(1)
       write(label,'(A39)') '\fiM\fn\dW\u\(0248)4 detection boundary'
       call pgptxt(smax-0.09*(smax-smin),Pmax-0.10*(Pmax-Pmin),
     $             0.0,1.0,label)
       call pgsls(1)
       call pgsch(1.00)
c
       call pgend
c
1000   continue
       stop
       end
