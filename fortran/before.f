       program before
c
       integer i, n
       character*1 x, yn
       character*80 junk
c parse command line
       if ( IARGC() .ne. 1 ) then
        call banner
        goto 500
       else
        call getarg(1,x)
       end if
c count lines in file
       n = -1
       open ( unit = 12, file="detect.txt" )
       do i = 1, 10000
        read(unit=12,fmt='(A1,A80)',end=400) yn, junk
        if ( x .eq. 'N' .and. yn .eq. 'X' ) then
         continue
        else
         n = n + 1
        end if
       end do
400    rewind(12)
c file gets closed in munge
       call munge(n,x)
c
500    continue
       stop
       end
c
c the guts below simply need the file size and the cli info
       subroutine munge(n,x)
c
       integer i, n, s4
       real m, de
       real lbeta(5)
       character*1 yn, x
       character*3 momnt
       character*80 junk
c read and dump header
       read ( 12,* ) junk
       open ( unit = 14, file='detect.dat' )
c
       i = 0
100    format(A1,F7.2,F4.1,A4,A80)
       do while ( i .lt. n )
        read ( 12, 100 ) yn, de, m, momnt, junk
        if ( x .eq. 'N' .and. yn .eq. 'X' ) then
         goto 900
        else
         i = i + 1
         if ( momnt .eq. ' mb' ) then
c mb conversion from Lolli et al doi:10.1093/gji/ggu385
c          m = exp(0.058+0.284*m) + 0.770
c or Das 2012 doi:10.1016/j.seaes.2012.01.014
          m = 0.878*m + 0.653
          m = 1.3*m - 1.4
         else if ( momnt .eq. ' ml' ) then
c ml conversion from ml from Scordilis et al. 2013 Bull. Geol. Soc. Greece
c          m = m + 0.04
c or Scordilis 2006 doi:10.1007/s10950-006-9012-4 along with Lolli or Das
          m = 1.27*(m-1)-0.016*m*m
          m = 0.63*m + 2.5
          m = (m+2.2)/1.5
c          m = exp(0.058+0.284*m) + 0.770
          m = 0.878*m + 0.653
          m = 1.3*m - 1.4
         else if ( momnt .eq. ' md' ) then
c md conversion from 2016 doi:10.3906/yer-1511-7
          m = 0.7947*m + 1.3420
c          m = m*0.936
c          m = m + 0.04
         end if
         if ( yn .eq. 'Y' ) then
          write(14,'(I1,F7.2,F4.1)') 1,de,m
         else if ( yn .eq. 'N' ) then
          write(14,'(I1,F7.2,F4.1)') 0,de,m
         else if ( yn .eq. 'X' ) then
c X label is for data compromised in other ways---unable to say Y or N
          write(14,'(I1,F7.2,F4.1)') 0,de,m
         else
          goto 1000
         end if
        end if
900     continue
       end do
       close(12)
       close(14)
c
1000   continue
       stop
       end
c 
c subroutines
       subroutine banner
       write(6,*) ''
       write(6,*) 'Usage: a.out X'
       write(6,*) 'X = Y to count no decisions as non-detections'
       write(6,*) 'X = N to not count them at all'
       write(6,*) ''
       end
c end of file
