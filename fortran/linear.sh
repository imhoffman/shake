#!/bin/sh
if [ "$#" -ne 2 ]; then
 echo "\n Usage linear.sh Y/N F/T\n"
 exit 1
else
 ./before $1
 if [ $2 = 'F' ]; then
  octave -p ../matlab ../matlab/sensl.m 0
 elif [ $2 = 'T' ]; then
  octave -p ../matlab ../matlab/sensl.m 10
 else
  echo "\n Usage linear.sh Y/N F/T\n"
  exit 1
 fi
 ./testingl
 mv four.png ../images/four.png
 ./afterl N $1 0.33
 mv out.ps /tmp/1.ps
 ./afterl L $1 0.33
 mv out.ps /tmp/2.ps
 ps2pdf /tmp/1.ps /tmp/1.pdf
 ps2pdf /tmp/2.ps /tmp/2.pdf
 gs -dNOPAUSE -dQUIET -dBATCH -sDEVICE=pdfwrite -sOutputFile=../images/linear.pdf /tmp/1.pdf /tmp/2.pdf
 gv --scale=2.0 ../images/linear.pdf &
 exit 0
fi
