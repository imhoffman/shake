#!/bin/sh
./before N
echo ''
echo 'Linear boundary with testing'
octave -p ../matlab ../matlab/sensl.m 10
echo ''
echo 'Linear boundary without testing'
octave -p ../matlab ../matlab/sensl.m 0
./afterl N N 0.33
mv out.ps /tmp/1.ps
./afterl L N 0.33
mv out.ps /tmp/2.ps
ps2pdf /tmp/1.ps /tmp/1.pdf
ps2pdf /tmp/2.ps /tmp/2.pdf
gs -dNOPAUSE -dQUIET -dBATCH -sDEVICE=pdfwrite -sOutputFile=../images/linear.pdf /tmp/1.pdf /tmp/2.pdf
#
echo ''
echo 'Parabolic boundary with testing'
octave -p ../matlab ../matlab/sens.m 10
echo ''
echo 'Parabolic boundary without testing'
octave -p ../matlab ../matlab/sens.m 0
./after N N 0.33
mv out.ps /tmp/1.ps
./after L N 0.33
mv out.ps /tmp/2.ps
ps2pdf /tmp/1.ps /tmp/1.pdf
ps2pdf /tmp/2.ps /tmp/2.pdf
gs -dNOPAUSE -dQUIET -dBATCH -sDEVICE=pdfwrite -sOutputFile=../images/poly.pdf /tmp/1.pdf /tmp/2.pdf
./testing
convert -rotate 90 -density 200 -alpha deactivate -background white -flatten four.ps ../images/four.png
