       program front
c
       integer i, n
       real P
       character*1 choice, x, yn
       character*10 arg
       character*80 junk
c parse command line
       if ( IARGC() .ne. 3 ) then
        call banner
        goto 500
       else
        call getarg(1,choice)
        call getarg(2,x)
        call getarg(3,arg)
        read (arg,'(F10.10)') P
       end if
c count lines in file
       n = -1
       open ( unit = 12, file="detect.txt" )
       do i = 1, 10000
        read(unit=12,fmt='(A1,A80)',end=400) yn, junk
        if ( x .eq. 'N' .and. yn .eq. 'X' ) then
         continue
        else
         n = n + 1
        end if
       end do
400    rewind(12)
c file gets closed in sens
       call sens(n,choice,x,P)
c
500    continue
       stop
       end
c
c the guts below simply need the file size and the cli info
       subroutine sens(n,choice,x,P)
       use logistic_regression
c
       integer pgbeg, i, n, s4
       parameter ( k = 2048, kk = 26 )
       real a, m, de, dmin, dmax, smin, smax, mmin, mmax, d4, P, P4
       real model(k), deg(k), mag(k), logit(k)
       real legx(2), legy(2), s(kk), theta(kk)
       real*8 lx(n,2), lchisq, ldev, lbeta(3), lsebeta(3) 
       integer lk, ls(n), ln(n), lndf, lier
       character*1 yn, choice, x
       character*3 momnt
       character*5 str
       character*10 dist, arg
       character*22 label
       character*27 shake, shake2
       character*80 junk
c constants
       dmin = -08.0
       dmax = 169.0
       mmin = +0.95
       mmax = 8.19
       a = 16.05
c distance axis
       do i = 1, kk
        s(i) =  0.0+ real(i-1)/real(kk)*13000.0
        theta(i) = 2.0*asin(s(i)/2.0/6400.0)*180.0/3.14159
        if ( choice .eq. 'L' ) then
         theta(i) = log10(theta(i))
        end if
       end do
c read and dump header
       read ( 12,* ) junk
c
c  ---=== plotting ===---
c
c begin pgplot
c        if ( pgbeg(0,'/XWINDOW',1,1) .ne. 1 ) stop
        if ( pgbeg(0,'out.ps/CPS',1,1) .ne. 1 ) stop
c        if ( pgbeg(0,'out.ps/VCPS',1,1) .ne. 1 ) stop
c        if ( pgbeg(0,'out.gif/GIF',1,1) .ne. 1 ) stop
c        if ( pgbeg(0,'out.png/PNG',1,1) .ne. 1 ) stop
c       call pgask ( .false. )
c
       call pgsvp(  0.15, 0.90, 0.15, 0.85 )
       call pgslw(3)
       call pgsch(1.5)
       if ( choice .eq. 'L' ) then
        call pgswin(  -0.18,  log10(dmax), mmin, mmax )
       else if ( choice .eq. 'N' ) then
        call pgswin(  dmin,  dmax,  mmin, mmax )
       else
        call banner
        goto 1000
       end if
        call pglab (
     $           'angular distance from hypocenter (degrees)',
     $           'magnitude of event (\fiM\fn\dW\u)',
     $           '')
c
       call pgscr(03, 0.00, 0.75, 0.00)
       call pgsls(1)
       call pgslw(4)
       call pgsch(1.0)
c
       i = 0
100    format(A1,F7.2,F4.1,A4,A80)
       do while ( i .lt. n )
        read ( 12, 100 ) yn, de, m, momnt, junk
        if ( x .eq. 'N' .and. yn .eq. 'X' ) then
         goto 900
        else
         i = i + 1
         ln(i) = 1
c feed log angle to logistic since decision boundary is linear
         lx(i,2) = log10(de)
         if ( choice .eq. 'L' ) then
          de = log10(de)
         end if
         if ( momnt .eq. ' mb' ) then
c mb conversion from Lolli et al doi:10.1093/gji/ggu385
c          m = exp(0.058+0.284*m) + 0.770
c or Das 2012 doi:10.1016/j.seaes.2012.01.014
          m = 0.878*m + 0.653
          m = 1.3*m - 1.4
         else if ( momnt .eq. ' ml' ) then
c ml conversion from ml from Scordilis et al. 2013 Bull. Geol. Soc. Greece
c          m = m + 0.04
c or Scordilis 2006 doi:10.1007/s10950-006-9012-4 along with Lolli or Das
          m = 1.27*(m-1)-0.016*m*m
          m = 0.63*m + 2.5
          m = (m+2.2)/1.5
c          m = exp(0.058+0.284*m) + 0.770
          m = 0.878*m + 0.653
          m = 1.3*m - 1.4
         else if ( momnt .eq. ' md' ) then
c md conversion from 2016 doi:10.3906/yer-1511-7
          m = 0.7947*m + 1.3420
c          m = m*0.936
c          m = m + 0.04
         end if
         lx(i,1) = m
         if ( yn .eq. 'Y' ) then
          call pgsci(4)
          call pgpt1(de,m,12)
          ls(i) = 1
         else if ( yn .eq. 'N' ) then
          call pgsci(2)
          call pgpt1(de,m,21)
          ls(i) = 0
         else if ( yn .eq. 'X' ) then
c X label is for data compromised in other ways---unable to say Y or N
          call pgsci(1)
          call pgpt1(de,m,5)
          ls(i) = 0
         else
          goto 1000
         end if
        end if
900     continue
       end do
       close(12)
c logistic fit from Alan Miller
       call logistic(n,lx,2,ls,ln,lchisq,ldev,lndf,lbeta,lsebeta,lier)
       if ( lier .ne. 0 ) then
        write(6,*) 'fit failure'
        goto 1000
       end if
c model curves
       do i = 1, k
        deg(i) = 1.0E-6 + real(i)/real(k)*180.0
        mag(i) = 1.0 + real(i)/real(k)*8.0
        drad = deg(i)*3.14159/180.0
        model(i) = a + (4.0/3.0)*log10( sin(drad/2.0) ) - 10.7
        logit(i) =( log(P/(1.0-P)) - lbeta(1) - lbeta(3)*log10(deg(i)) )
     $              /lbeta(2)
        if ( choice .eq. 'L' ) then
         deg(i) = log10(deg(i))
        end if
       end do
       call pgsls(2)
       call pgslw(3)
       call pgsci(03)
       call pgline(k,deg,model)
       call pgsci(08)
       call pgline(k,deg,logit)
c Shake spec (only for log display---crowding)
       if ( choice .eq. 'L' ) then
        if ( x .eq. 'Y' ) then
         P4 = 0.75
         d4 = 10**((log(P4/(1.0-P4))-lbeta(1)-lbeta(2)*4.0)/lbeta(3))
     $          *3.14159/180.0
         s4 = int(2.0*6400.0*sin(d4/2.0))
        else
         P4 = 0.99
         d4 = 10**((log(P4/(1.0-P4))-lbeta(1)-lbeta(2)*4.0)/lbeta(3))
     $          *3.14159/180.0
         s4 = int(2.0*6400.0*sin(d4/2.0))
        end if
        s4 = s4 - mod(s4,10)
        write(dist,'(A3,I4,A3)') 'at ',s4, ' km'
        d4 = d4*180.0/3.14159
        call pgsch(1.0)
        d4 = log10(d4)
        call pgsci(6)
        call pgpt1(d4,mmax,11)
        write(shake,'(I2,A25)') int(P4*100.0),
     $               '% of \fiM\fn\dW\u\(0248)4'
        call pgptxt(d4,mmax-0.07*(mmax-mmin),
     $             0.0,0.5,shake)
        call pgptxt(d4,mmax-0.13*(mmax-mmin),
     $             0.0,0.5,dist)
        call pgsci(08)
        write(shake2,'(I2,A25)') int(P*100.0),
     $               '% of \fiM\fn\dW\u\(0248)4'
        d4 = (log(P/(1.0-P))-lbeta(1)-lbeta(2)*4.0)/lbeta(3)
        call pgpt1(d4,mmax,11)
        call pgptxt(d4,mmax+0.03*(mmax-mmin),
     $             0.0,0.5,shake2)
       end if
c non-linear distance axis
       call pgsci(1)
       call pgsch(1.50)
       do i = 1, kk
        call pgpt1(theta(i),mmax,0808)
        if ( i.eq.1 .or. (mod(i,10).eq.1) ) then
         call pgpt1(theta(i),mmax,0798)
         write(str,'(I5)') int(s(i))
         call pgptxt(theta(i),mmax+0.05*(mmax-mmin),0.0,0.89,str)
        end if
       end do
       if ( choice .eq. 'L' ) then
        call pgptxt((log10(dmax)-0.15)/2.0,mmax+0.14*(mmax-mmin),0.0,0.5
     $      ,'linear distance from epicenter (km)')
       else
        call pgptxt((dmin+dmax)/2.0,mmax+0.14*(mmax-mmin),0.0,0.5,
     $      'linear distance from epicenter (km)')
       end if
c frame and labels
       call pgsch(1.50)
       call pgsls(1)
       call pgsci(1)
       call pgslw(3)
       if ( choice .eq. 'L' ) then
        call pgbox ( 'bnlts', 0.0, 0, 'bcnmtsv', 1.0, 2 )
       else
        call pgbox ( 'bnts', 0.0, 0, 'bcnmtsv', 1.0, 2 )
       end if
       call pgsch(1.00)
c other horizontal axis and legend
       smin = dmin*3.14159/180.0*6400.0
       smax = dmax*3.14159/180.0*6400.0
       call pgslw(3)
       call pgsch(1.5)
       call pgswin(  smin,  smax, mmin , mmax )
       call pgbox ( 'c', 0.0, 0, '', 0.0, 0 )
       call pgsch(1.0)
       call pgsci(1)
       if ( x .eq. 'Y' ) then
        call pgptxt(smax-0.06*(smax-smin),mmin+0.32*(mmax-mmin),
     $             0.0,0.5,'\m5')
        call pgptxt(smax-0.08*(smax-smin),mmin+0.32*(mmax-mmin),
     $             0.0,1.0,'no decision')
       end if
       call pgptxt(smax-0.06*(smax-smin),mmin+0.26*(mmax-mmin),
     $             0.0,0.5,'\m12')
       call pgptxt(smax-0.08*(smax-smin),mmin+0.26*(mmax-mmin),
     $             0.0,1.0,'detection')
       call pgptxt(smax-0.06*(smax-smin),mmin+0.20*(mmax-mmin),
     $             0.0,0.5,'\m21')
       call pgptxt(smax-0.08*(smax-smin),mmin+0.20*(mmax-mmin),
     $             0.0,1.0,'non-detection')
       call pgsci(03)
       legy(1) = mmin+0.15*(mmax-mmin)
       legy(2) = legy(1)
       legx(1) = smax-(0.06+0.01)*(smax-smin)
       legx(2) = smax-(0.06-0.01)*(smax-smin)
       call pgline(2,legx,legy)
       call pgsci(1)
       call pgptxt(smax-0.08*(smax-smin),mmin+0.14*(mmax-mmin),
     $             0.0,1.0,'inverse-square model')
       call pgsci(08)
       legy(1) = mmin+0.09*(mmax-mmin)
       legy(2) = legy(1)
       legx(1) = smax-(0.06+0.01)*(smax-smin)
       legx(2) = smax-(0.06-0.01)*(smax-smin)
       call pgline(2,legx,legy)
       call pgsci(1)
       write(label,'(I2,A20)') int(P*100.0), '% detection boundary'
       call pgptxt(smax-0.08*(smax-smin),mmin+0.08*(mmax-mmin),
     $             0.0,1.0,label)
       call pgsls(1)
       call pgsch(1.00)
c
       call pgend
c
1000   continue
       stop
       end
c 
c subroutines
       subroutine banner
       write(6,*) ''
       write(6,*) 'Usage: a.out C X P'
       write(6,*) 'C = N for normal'
       write(6,*) 'C = L for log'
       write(6,*) 'X = Y to count no decisions as non-detections'
       write(6,*) 'X = N to not count them at all'
       write(6,*) 'P = probability boundary for logistic fit'
       write(6,*) ''
       end
c end of file
