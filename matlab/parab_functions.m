1;

function f = sigmoid(z)
  f = 1.0 ./ (1.0 + exp(-z));
end

function f = h(A,B)
  f = sigmoid( A * B );
end

function f = S(C,D,G)
% C is X matrix of features
% D is y vector of true outcomes
% G is beta vector of coefficients
 m = length(D);
 total = 0.0;
 for j = 1:m
  total += -1.0*( D(j)*log( h(C(j,:),G) ) + (1.0-D(j))*log( 1.0 - h(C(j,:),G) ) );
 endfor
% if ( isnan(total) )
%  f = 1000.0;
% else
  f = total/m;
% endif
end

