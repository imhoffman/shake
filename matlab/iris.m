pkg load signal
daystring = input('Event UT date (in quotes) as YYYY-MM-DD HH:MM:SS:  ');
daynum = datenum(daystring);
dayfrac = mod( daynum, 1 );
dayof2017 = ceil( daynum - datenum('2017-01-01 00:00:00') );
filename = sprintf('../2017/AM/RBF6B/SHZ.D/AM.RBF6B.00.SHZ.D.2017.%3d',dayof2017);
lat = input('Event latitude (decimal deg, North is positive):  ');
lon = input('Event longitude (decimal deg, East is positive):  ');
hkm = input('Event depth (km, down is positive):  ');
urlstring = sprintf('http://service.iris.edu/irisws/traveltime/1/query?evdepth=%f&noheader=true&evloc=[%f,%f]&staloc=[49.7368,-123.0951]',hkm,lat,lon);
#urlstring = sprintf('http://service.iris.edu/irisws/traveltime/1/query?phases=P,pP,PP,PcP,S,sP,sS,SS,ScS,PKiKP,SKiKS,SKS,PKP,Pdiff&evdepth=%f&noheader=true&evloc=[%f,%f]&staloc=[49.738,-123.101]',hkm,lat,lon);
#
# parse web calculator
webpage = urlread(urlstring);
scan = textscan(webpage,'%f %f %s %f %f %f %f %f %s %s\n');
deg = scan{1,1};
depth = scan{1,2};
# labels is a cell array of strings, access as labels{n,m}
labels = scan{1,3};
tarr = scan{1,4};
tarr = tarr/86400.0 + dayfrac;
#
# read in appropriate miniSEED file (takes a while)
X = rdmseed(filename);
signal = ([ cat(1,X.t)-floor(daynum) cat(1,X.d) ]);
tmins = ( signal(:,1) - dayfrac )*60.0*24.0;
#
# filtering
# Nyquist in units of Hz
fNy = 1.0/( signal(4,1)-signal(3,1) )/86400.0/2.0;
# instrument response
resp = dlmread('response_stock.fir');
decim = 4;
[h,f] = freqz(resp,1024,[],fNy*2.0*decim);	% freq resp of filter
H = impz(resp,1,256,fNy*2.0*decim)';		% time resp of filter
%in = deconv( signal(:,2), H );
in = signal(:,2);
# https://www.allaboutcircuits.com/technical-articles/design-of-fir-filters-design-octave-matlab/
# octave filter, edges in Hz
fhi = 6.50; 					% low-pass cut-off
d = fir1( 33, fhi/fNy, 'low' );
#d = fir1( 129, [ 0.20/fNy, 7.25/fNy ], 'bandpass' );
#d = fir2( 129, [ 0, 0.50/fNy, 0.50/fNy, 7.5/fNy, 7.5/fNy, 1 ], ...
#               [ 0,    0.0  ,    1.0  ,   0.75 ,  0.0   , 0 ]);
bp = filter( d, 1, in );
#
# plotting
figure('position',[47, 175, 1760, 800])
# number of minutes to plot preceding and following the first arrival
minmin = 2.0;
minmax = 16.0;
#
subplot(2,1,2)
yavg = mean( bp );
plot( signal(:,1), in, 'LineStyle','-', 'Color',[0.75,0.95,0.75] )
hold on
plot( signal(:,1), bp, 'b-' )
hold off
xlim([tarr(1)-minmin/60.0/24.0 tarr(1)+minmax/60.0/24.0])
ylim( [ yavg-200.0 yavg+200.0 ] )
set(gca,'fontsize',18)
hold on
for i = 1:length(tarr)
  line( [tarr(i) tarr(i)], ylim, 'Color','red','LineStyle','--' )
  text( tarr(i), (mod(i,4)+1)*0.20*(max(ylim)-min(ylim))+min(ylim), labels{i,1}, 'FontSize', 24 )
end
hold off
set(gca,'fontsize',18)
xstr = sprintf('Fraction of day %3d',dayof2017);
xlabel(xstr)
ystring = sprintf('counts (after %.1f-Hz low pass)', fhi);
ylabel(ystring)
#
subplot(2,1,1)
window = ceil(50.0*fNy*2.0);	# get down to 0.04 Hz
step = ceil(5.0*fNy*2.0);
[S,f,t] = specgram(bp,8192,2.0*fNy,window,window-step);
t = t/86400.0 + window/fNy*1.0/2.0/86400.0 + signal(1,1);
idx = (f >= 0.01 & f <= fhi);
imagesc( t,f, log(abs( S(idx,:) ))  )
#set(gca,'ydir','normal','fontsize',18,'YTick',[0.0:1.00:fhi],'YMinorTick','off')
set(gca,'ydir','normal','fontsize',18,'YMinorTick','on')
colormap jet
ylim([0.08 fhi])
xlim([tarr(1)-minmin/60.0/24.0 tarr(1)+minmax/60.0/24.0])
#xlabel(xstr)
ylabel('frequency (Hz)')
#
datestr = daystring(1:10);
titlestr = sprintf('Event at %s UTC, %.4g degrees away, %.1f km deep', daystring, deg(1), depth(1));
title(titlestr,'fontsize',24)
#EOF
