pkg load signal
% http://manual.raspberryshake.org/specifications.html#raspberry-shake-1d-instrument-response-files
resp1 = dlmread('response_stock.fir');	% obtained from instruction manual
resp2 = dlmread('response.fir');	% obtained from stationview
[h1,f1] = freqz(resp1,1024,[],200.0);	% 50-Hz system is 200 Hz decimated by 4
[h2,f2] = freqz(resp2,1024,[],200.0);	% 1024 > 53 and 1024 > 105, 128 would do
figure();
h1 = h1./max(h1);			% normalize
h2 = h2./max(h2);
plot(f1,abs(h1),'r-')
hold on
plot(f2,abs(h2),'b-')
hold off
xlim([0.0 41.0])
ylim([0.0  1.1])
set(gca,'fontsize',18)
ylabel('normalized instrument response','fontsize',18)
xlabel('frequency (Hz)','fontsize',18)
legend('estimated nominal','actual shipped','Location','southwest')
