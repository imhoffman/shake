warning('off', 'Octave:possible-matlab-short-circuit-operator');
parab_functions				% sigmoid, hypothesis, and cost
args = argv();
frac = str2num( args{1} );
%
alpha = 1.0E-01;			% learning rate
tol = 3.0E-08;				% iteration difference gain
max_iter = 400000;
input = dlmread('detect.dat','',0,0);	% from before.f
n = 4;					% number of features
%
if ( frac >= 1 )		% obtain random test sample of portion of set
 mm = ceil( rand(1)*(frac-1.0) );
 j = 0;
 k = 0;
 for i = 1:length(input)
  if ( mod(i,frac) - mm == 0 )
   j = j + 1;
   ytest(j) = input(i,1);
   degtest(j) = log10( input(i,2) );
   magtest(j) = input(i,3);
  else
   k = k + 1;
   y(k) = input(i,1);			% the one-or-zero detections
   X(k,1) = 1.0 ;			% the constant intercept
   X(k,2) = log10( input(i,2) );	% log angle b/c power law expected
   X(k,3) = X(k,2) .* X(k,2) ;		% its quadratic feature
   X(k,4) = input(i,3) ;		% magnitude
   X(k,5) = X(k,4) .* X(k,4) ;		% its quadratic feature
  end
 end
 ytest = ytest';			% make into columns
 y = y';
 m = length(y);
else
 y = input(:,1);			% the one-or-zero detections
 m = length(y);
 X = ones(m,n+1) ;			% the constant intercept
 X(:,2) = log10( input(:,2) );		% log angle b/c power law expected
 X(:,3) = X(:,2) .* X(:,2) ;		% its quadratic feature
 X(:,4) = input(:,3) ;			% magnitude
 X(:,5) = X(:,4) .* X(:,4) ;		% its quadratic feature
 ytest = 0;
end
%
beta = ones(n+1,1);
temp = ones(n+1,1);
scale = ones(n+1,1);
nn = 100;
errs = zeros(n+1,nn);			% keep last nn iterations
for i = 1:n+1
 scale(i) = max(X(:,i),[],1);		% work with features near unity
 X(:,i) = X(:,i) ./ scale(i);
end
gain = 2.0 * tol ;
pct1 = 0.0;
count = 0;
% the iterative loop
while ( (gain > tol) & (count < max_iter) )
 pct0 = pct1;
 for i = 2:nn
  errs(:,i-1) = errs(:,i);		% poor man's sensitivity tally
 end
 for i = 1:n+1
  temp(i) = beta(i) - alpha/m*( (h(X,beta)-y)'*X(:,i) );
  errs(i,nn) = temp(i);
 end
 beta = temp;				% update
 count = count + 1;
 pct1 = 1.0 - sum( abs( h(X,beta) - y ) )/m ;	% simple metric
 if ( pct1 < pct0 )
  printf('iterations getting worse; count = %d\n', count);
 end
 gain = abs( pct1 - pct0 ) ;
end
% end loop
if ( length(ytest) > 1 )		% test model, if requested
 correct = 0;
 wrong = 0;
 for i = 1:length(ytest)
  Xtest = ([1.0,degtest(i),degtest(i)*degtest(i),magtest(i),magtest(i)*magtest(i)]);
  if ( h(Xtest,beta) > ytest(i)  ||  h(Xtest,beta) < ytest(i) )
   correct = correct + 1;
  else
   wrong = wrong + 1;
  end
 end
 if ( correct + wrong != length(ytest) )
  printf('Problem with test.\n');
 end
end
%
printf('Completed in %d iterations.\n', count);
if ( length(ytest) > 1 )
 printf('Training set has %d events including %d detections.\n', length(y), sum(y));
 printf(' Random test set has %d events including %d detections.\n', length(ytest), sum(ytest));
 printf(' Test score: %.1f%%\n', correct/length(ytest)*100.0);
else
 printf('Processed all %d events including %d detections.\n', length(y), sum(y));
end
for i = 1:n+1
 err(i) = std( errs(i,:) )*m/alpha/abs(beta(i));
end
err = err' ;
beta = beta ./ scale;
printf('Coefficients and estimated fractional uncertainty:\n');
printf('     constant: %+.2E %.1E\n', beta(1), err(1) );
printf('   log(angle): %+.2E %.1E\n', beta(2), err(2) );
printf(' log(angle)^2: %+.2E %.1E\n', beta(3), err(3) );
printf('    magnitude: %+.2E %.1E\n', beta(4), err(4) );
printf('  magnitude^2: %+.2E %.1E\n', beta(5), err(5) );
save -ascii beta.dat beta		% write file for after.f
save -ascii err.dat err			% write file for testing.f
