h = gcf;
set(h, 'PaperOrientation','landscape');
set(h, 'PaperUnits','normalized');
set(h, 'PaperPosition',[0 0 1 1]);
print(gcf, '-dpdf', 'out.pdf');
